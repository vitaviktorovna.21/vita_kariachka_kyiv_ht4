﻿using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumGridHT5.Models;
using SeleniumGridHT5.Services;

namespace SeleniumGridHT5
{
    public class BaseTest
    {
        public ThreadLocal<IWebDriver> Driver  { get; private set; } = new ThreadLocal<IWebDriver>();

        private string hubUrl = "http://localhost:4444/wd/hub";

        [SetUp]
        public void Setup()
        {
            Driver.Value = LocalDriverFactory.CreateInstance(BrowserType.Chrome);
        }

        [TearDown]
        protected void TearDown()
        {
            Driver.Value.Quit();
        }
    }
}
