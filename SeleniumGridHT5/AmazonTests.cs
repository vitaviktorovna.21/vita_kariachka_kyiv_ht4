using System;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
namespace SeleniumGridHT5
{
    [Parallelizable(ParallelScope.All)]
    public class Tests : BaseTest
    {
        [Test]
        public void OpenAmazonAndSearch()
        {
            var driver = Driver.Value;

            driver.Navigate().GoToUrl("https://www.amazon.com/");
            driver.Manage().Window.Maximize();
            driver.FindElement(By.XPath("//div[@class='nav-search-field ']//input[@class='nav-input nav-progressive-attribute']")).SendKeys("iphone 13");
            driver.FindElement(By.XPath("//*[@id='nav-search-submit-button']")).Click();

            Assert.IsTrue(driver.Url.Contains("iphone"));
        }

        [Test]
        public void OpenBingAndSearch()
        {
            var searchResultCount = 48;
            var driver = Driver.Value;

            driver.Navigate().GoToUrl("https://www.amazon.com/");
            driver.Manage().Window.Maximize();
            driver.FindElement(By.XPath("//div[@id='desktop-grid-6']//div[@class='a-cardui fluid-fat-image-link fluid-card fluid-fat-image-link']")).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            var elementsList = driver.FindElements(By.XPath("//div[@class='a-section a-spacing-base']"));
            var actualElementsSize = elementsList.Count();

            Assert.AreEqual(actualElementsSize, searchResultCount);
        }
    }
}